import { AppRegistry } from 'react-native';
import Routing from './app/app';


XMLHttpRequest = GLOBAL.originalXMLHttpRequest ? GLOBAL.originalXMLHttpRequest : GLOBAL.XMLHttpRequest;

AppRegistry.registerComponent('DAR', () => Routing);
import React, { Component } from 'react';
import { DrawerNavigator } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleIcons from 'react-native-vector-icons/SimpleLineIcons';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  StatusBar,
  ScrollView,
  TouchableNativeFeedback,
} from 'react-native';
import { 
  Button,
  SocialIcon,
  Input
} from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import API_URL from '../../components';

import { color } from 'react-native-material-design-styles';

export default class SignUpScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      form: {
        file: {}
      }
    }
  }
  SelectImage() {
    ImagePicker.openPicker({
        width: 480,
        height: 480,
        cropping: true
    }).then(image => {
      this.setState({
        form: {
          ...this.state.form,
          file: { 
            name: 'photoProfile.jpg',
            type: image.mime, 
            uri: image.path 
          }
        }
      })
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView style={styles.scrollView}>
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <View style={styles.container}>           
          <Image
            resizeMode={'center'}
            style={{width: 280, height:280}}
            source={require("../../resource/img/logorojo.png")}
          />
          <View style={styles.content}>
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <SimpleIcons
                    name='user'
                    size={16}
                    color='white'
                  />
                }
                onChangeText={(text) => this.setState({
                  form: {
                    ...this.state.form,
                    name: text,
                  }
                })}
								value={this.state.form.name}
                placeholder="Nombre"
              />
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='email'
                    size={16}
                    color='white'
                  />
                }
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										mail: text,
									}
								})}
								value={this.state.form.mail}
								keyboardType={'email-address'}
                placeholder="Correo Electronico"
              />
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='phone-android'
                    size={16}
                    color='white'
                  />
                }
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										phone: text,
									}
								})}
								keyboardType={'phone-pad'}
								value={this.state.form.phone}
                placeholder="Numero de Telefono"
              />
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='credit-card'
                    size={16}
                    color='white'
                  />
                }
                onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										rfc: text,
									}
								})}
								keyboardType={'phone-pad'}
								value={this.state.form.rfc}
                placeholder="RFC"  
              />
              <Input
                editable = {true}
                maxLength = {300}
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='location-on'
                    size={16}
                    color='white'
                  />
                }
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										address: text,
									}
								})}
								value={this.state.form.address}
                placeholder="Direccion"
              />
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='lock'
                    size={16}
                    color='white'
                  />
                }
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										password: text,
									}
								})}
								value={this.state.form.password}
                placeholder="Contraseña"
                secureTextEntry={true}
              />
              <Input
                placeholderTextColor='#FFF'
                containerStyle={styles.containerStyle}
                inputStyle={styles.inputStyle}
                color="#FFF"
                shake
                leftIcon={
                  <MaterialIcons
                    name='lock'
                    size={16}
                    color='white'
                  />
                }
								onChangeText={(text) => this.setState({
									form: {
										...this.state.form,
										Confirm_password: text,
									}
								})}
								value={this.state.form.Confirm_password}
                placeholder="Confirmar Contraseña"
                secureTextEntry={true}
              />
              <View style={styles.contentBtn}>
                <TouchableNativeFeedback  onPress={() => this.SelectImage()}>
                    <View style={styles.btn}>
                        <Text style={styles.btn_text}>Imagen de Perfil</Text>
                    </View>
                </TouchableNativeFeedback>   
                {
                  this.state.form.file.uri
                  &&
                  <Image
                    resizeMode={'cover'}
                    style={{width: 250, height: 250, borderRadius: 6}}
                    source={{uri: this.state.form.file.uri}}
                  />
                }
                <TouchableNativeFeedback onPress={() => this.handlerSubmit()}>
                    <View style={styles.btn}>
                        <Text style={styles.btn_text}>Registrar</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback  onPress={() => navigate('Login')}>
                    <View style={styles.btn}>
                        <Text style={styles.btn_text}>Iniciar Sesion</Text>
                    </View> 
                </TouchableNativeFeedback>
              </View>
          </View>
        </View>
      </ScrollView>
    );
  }
  OnAuth() {
    this.props.navigation.navigate('Home');
    console.log(this.state)
    
    // AsyncStorage.getItem('@app:session')
    //  .then(token => {
    //    console.log(token);
    //  })
    // AsyncStorage.setItem(
    //   '@app:session', '1a45b5t9',
    //   error => {
    //     console.log(error)
    //    if(!error)
    //   }
    // );
  }
}

const styles = StyleSheet.create({
	scrollView: {
    flex: 1,
    backgroundColor: '#17afbd',
	},
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#17afbd',
  },
  content: {
    marginTop: 10,
    width: 250,
    height: 'auto',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    color: 'white',
  },
  containerStyle: {
    width: 'auto',
    height: 52,
    marginLeft: 0,
    marginBottom: 6,
    borderRadius: 5,
    backgroundColor: '#99e5f3',
    borderBottomWidth: 0,
    borderBottomColor: 'transparent',
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    padding: 5,
  },
  contentBtn: { 
    marginTop: 10,
    marginBottom: 10
  },
  btn: {
    flex: 1,
    position: 'relative',
    width: 250,
    height: 42,
    margin: 0,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    padding: 5,
  },
  btn_text: {
    color: '#e52427',
  }
})